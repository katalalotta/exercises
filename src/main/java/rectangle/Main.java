package rectangle;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws InterruptedException {
        takeUserInputAndDraw();
        //draw(7, 8, "*");

    }

    public static void draw(int h, int w, String s) {
        for (int i = 1; i <= h; i++) {
            for (int j = 1; j <= w; j++) {
                if (i==1 || i==h) {
                    System.out.print(s);
                } else if (j==1 || j==w){
                    System.out.print(s);
                } else{
                    System.out.print(" ");
                }
            }
            System.out.print("\n");
        }
    }
    public static void takeUserInputAndDraw() throws InterruptedException {
        Scanner s = new Scanner(System.in);
        int h;
        int w;
        String cha = "";
        System.out.println("Let's draw a rectangle with a single character.");
        while(true) {
            System.out.println("Enter the height:");
            try {
                h = Integer.parseInt(s.nextLine());
                break;
            } catch(NumberFormatException e){
                System.out.println("You should provide a number!");
            }
        }
        while(true) {
            System.out.println("Enter the widtht:");
            try {
                w = Integer.parseInt(s.nextLine());
                break;
            } catch(NumberFormatException e){
                System.out.println("You should provide a number!");
            }
        }

        System.out.println("Enter the character for drawing");
        cha = s.nextLine();
        Thread.sleep(300);
        System.out.println("Let's start!");
        Thread.sleep(3000);

        draw(h,w,cha);

    }

}