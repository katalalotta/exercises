package calculator;

public class InvalidDividerException extends Exception {

    public InvalidDividerException(String message){
        super(message);
    }
}
