package calculator;

public class Calculator {
    public static void main(String[] args) throws InvalidDividerException {
        Calculator c = new Calculator();
        System.out.println(c.divide(2,0));
    }

    public double add(double x, double y) {
        return x+y;
    }

    public double subtract(double x, double y) {
        return x-y;
    }

    public double multiply(double x, double y) {
        return x*y;
    }

    public double divide(double x, double y) throws InvalidDividerException {
        if (y==0){
            throw new InvalidDividerException("You can not divide by zero (0)!");
        }
        return x/y;
    }
}
