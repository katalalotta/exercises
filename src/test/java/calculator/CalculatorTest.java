package calculator;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CalculatorTest {

    @Test
    void testAddition(){
        Calculator c = new Calculator();
        double actual = c.add(-100,2);
        assertEquals(-98,actual);
    }

    @Test
    void testSubtraction(){
        Calculator c = new Calculator();
        double actual = c.subtract(1,2);
        assertEquals(-1,actual);
    }

    @Test
    void testMultiplication(){
        Calculator c = new Calculator();
        double actual = c.multiply(3,Math.pow(2,2));
        assertEquals(12,actual);
    }

    @Test
    void testDivision() throws InvalidDividerException {
        Calculator c = new Calculator();
        double actual = c.divide(3,2);
        assertEquals(1.5,actual);
    }

    @Test
    void testDividingWithZeroLeadsToException() throws InvalidDividerException{
        Calculator c = new Calculator();
        String expected = "You can not divide by zero (0)!";
        InvalidDividerException thrown = assertThrows(
                InvalidDividerException.class,
                () -> c.divide(4,0)
        );
        assertEquals(expected,thrown.getMessage());
    }
}